import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe("array map test", () => {
    // Please add test cases here
    test("Test: halfNumbers", () => {
        const numbers = [-3, -2, -1, 0, 1, 2, 3];

        const expected = [-1.5, -1, -0.5, 0, 0.5, 1, 1.5];
        const result = halfNumbers(numbers);

        expect(result).toEqual(expected);
    });

    test("Test: spliceNames", () => {
        const names = ["orange", true, 123];

        const expected = ["Hello orange", "Hello true", "Hello 123"];
        const result = spliceNames(names);

        expect(result).toEqual(expected);
    });
    
    test("Test: addSerialNumber", () => {
        const fruit = ["apple", "orange", "pear"];

        const expected = ["1. apple", "2. orange", "3. pear"];
        const result = addSerialNumber(fruit);

        expect(result).toEqual(expected);
    });
});
