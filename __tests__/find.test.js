import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";

describe("array find test", () => {
	test("Test: firstGrownUp: Found", () => {
		const ages = [12, 17, 18, 19, 30];
		
		const expected = 18;
		const result = firstGrownUp(ages);

		expect(result).toBe(expected);
	});

	test("Test: firstGrownUp: NotFound", () => {
		const ages = [12, 17, 1, 2];
		
		const result = firstGrownUp(ages);

		expect(result).toBeUndefined();
	});

    test("Test: firstOrange: Found", () => {
		const fruit = ['apple', 'pear', 'orange', 'banana'];
		
		const expected = 'orange';
		const result = firstOrange(fruit);

		expect(result).toBe(expected);
	});

    test("Test: firstOrange: NotFound", () => {
		const fruit = ['apple', 'pear', 'banana'];
		
		const result = firstOrange(fruit);

		expect(result).toBeUndefined();
	});

    test("Test: firstLengthOver5Name: Found", () => {
		const names = ['alan', 'ken', 'peter', 'rachel', 'steven'];
		
		const expected = 'rachel';
		const result = firstLengthOver5Name(names);

		expect(result).toBe(expected);
	});

    test("Test: firstLengthOver5Name: NotFound", () => {
		const names = ['alan', 'ken', 'peter', 'ivan'];
		
		const result = firstLengthOver5Name(names);

		expect(result).toBeUndefined();
	});
});
