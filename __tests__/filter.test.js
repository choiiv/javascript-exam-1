import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('array filter test', () => {
    // Please add test cases here
    test("Test: filterEvenNumbers", () => {
        const numbers = [1, 2, 3, 4, -1, -2, 0];

        const expected = [2, 4, -2, 0];
        const result = filterEvenNumbers(numbers);

        expect(result).toEqual(expected);
    });

    test("Test: filterLengthWith4", () => {
        const words = ["abcd", "", "abc", "abcde"];
        
        const expected = ["abcd"];
        const result = filterLengthWith4(words);

        expect(result).toEqual(expected);
    });

    test("Test: filterStartWithA", () => {
        const letters = ['a', 'aa', 'abcdefg', 'bcdef', 'bacd'];
        
        const expected = ['a', 'aa', 'abcdefg'];
        const result = filterStartWithA(letters);

        expect(result).toEqual(expected);
    });
});